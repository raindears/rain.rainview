Component: rainview
===================

A base class for RAIN client controllers based on Backbone.View, which ...

* provides a pretty sleek API for dealing with child views.
* brings the benefits of Backbone.js to the RAIN world.

###Requirements

[backbone](https://bitbucket.org/raindears/rain.backbone)
