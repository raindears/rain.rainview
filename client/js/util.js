define(function() {
  'use strict';

  /**
   * Loads and starts the controller of the requested child view.
   *
   * @memberOf util
   * @param {AsyncController} controller Controller of the view to load a child from
   * @param {String} sid Static ID of the child view (set in the view template)
   * @return {$.Deferred} A promise for the requested controller (is resolved when that controller is started).
   *
   * @see <a href="http://developers.1and1.com/hosting/rain/sdk/client/api/AsyncController.html">AsyncController</a>
   * @see <a href="http://api.jquery.com/category/deferred-object/">$.Deferred</a>
   */
  function getChildView(controller, sid) {
    var def = new $.Deferred();

    controller._getChild(sid).then(function(view) {
      var initialized = view.context.getRoot();

      if (initialized) {
        def.resolve(view);
      } else {
        view.on('start', def.resolve.bind(def, view));
        view.on('error', def.reject.bind(def));
      }
    }, def.reject);

    return def.promise();
  }

  /**
   * Static utility methods for dealing with RAIN views.
   *
   * @namespace util
   */
  return {

    getChildView: getChildView

  };

});
