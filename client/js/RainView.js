define([ 'underscore', 'backbone', '/rainview/1.0.0/js/util.js', 'raintime' ], function(_, Backbone, util, raintime) {
  'use strict';

  /* global ClientRenderer */

  var eventSplitter = /^(\w+)(\W+(\w+))?$/;

  /**
   * @name RainView
   * @class <a href="http://documentcloud.github.com/backbone/#View">Backbone.View</a> class for use with RAIN views.
   * @extends Backbone.View
   */
  var RainView = function() {
    Backbone.View.apply(this, arguments);
  };

  var eventMethods = [ 'on', 'off', 'trigger', 'once' ];

  RainView.prototype = _.extend(RainView.prototype, _.omit(Backbone.View.prototype, eventMethods),
  /** @lends RainView.prototype */
  {

    /**
     * The life-cycle hook for the "init" phase.
     *
     * <p>Sets the <code>cid</code> property of the <code>Backbone.View</code> to <code>"id;version: sid"</code>
     * (based on component info).</p>
     */
    init: function() {
      this._init();
    },

    _init: function() {
      var component = this.context.component;
      this.cid = (component.id + ';' + component.version + ': ' + component.sid);
    },

    /**
     * The life-cycle hook for the "start" phase.
     *
     * <p>Calls <code>Backbone.View.setElement()</code> with <code>this.context.getRoot()</code>.</p>
     */
    start: function() {
      this._start();
    },

    _start: function() {
      this.setElement(this.context.getRoot());
    },

    /**
     * The life-cycle hook for the "destroy" phase.
     *
     * <p>Calls <code>Backbone.View.remove()</code>, which stops listening to model/collection events.</p>
     */
    destroy: function() {
      this._destroy();
    },

    _destroy: function() {
      this.remove();
    },

    /**
     * Checks whether this view already passed its "start" life-cycle phase.
     *
     * @return {Boolean} <code>true</code> if the view is started.
     */
    isStarted : function() {
      return (this.context && this.context.getRoot());
    },

    /**
     * Loads and starts the controller of the requested child view.
     *
     * @param {AsyncController} [controller=this] Controller of the view to load a child from
     * @param {String} sid Static ID of the child view (set in the view template)
     * @param {Boolean|RainView.prototype} [asRainView] Whether to make that controller a {@link RainView}
     *     (or a specific one if a RainView prototype was given)
     * @return {$.Deferred} A promise for the requested controller (is resolved when that controller is started).
     */
    getChild: function(controller, sid, asRainView) {
      if (typeof controller === 'string') {
        asRainView = sid;
        sid = controller;
        controller = this;
      }

      return util.getChildView(controller, sid).pipe(function(view) {
        if (asRainView) {
          RainView.mixin(view, typeof asRainView === 'object' ? asRainView : null);
        }

        return view;
      });
    },

    /**
     * Replaces the contents of this view's DOM element (or a sub-element) with the given view.
     *
     * @param {Map} view See {@link http://developers.1and1.com/hosting/rain/sdk/client/api/Context.html#Context#insert}
     * @param {String} [selector] A CSS selector for a sub-element
     * @return {$.Deferred} A promise for the controller of the child view (is resolved when that controller is started)
     */
    insertChild: function(component, selector) {
      return this._setChild(component, selector);
    },

    /**
     * Appends the given view to the contents of this view's DOM element (or a sub-element).
     *
     * @param {Map} view See {@link http://developers.1and1.com/hosting/rain/sdk/client/api/Context.html#Context#insert}
     * @param {String} [selector] A CSS selector for a sub-element
     * @return {$.Deferred} A promise for the controller of the child view (is resolved when that controller is started)
     */
    appendChild: function(component, selector) {
      return this._setChild(component, selector, 'append');
    },

    /**
     * Prepends the given view to the contents of this view's DOM element (or a sub-element).
     *
     * @param {Map} view See {@link http://developers.1and1.com/hosting/rain/sdk/client/api/Context.html#Context#insert}
     * @param {String} [selector] A CSS selector for a sub-element
     * @return {$.Deferred} A promise for the controller of the child view (is resolved when that controller is started)
     */
    prependChild: function(component, selector) {
      return this._setChild(component, selector, 'prepend');
    },

    _setChild: function(component, selector, mode) {
      var clientRenderer = ClientRenderer.get()
        , context = this.context
        , instanceId = (Date.now() + (++clientRenderer.counter) + (component.sid || '') + context.instanceId)
        , $el = selector ? this.$(selector) : this.$el;

      $el[mode || 'html']('<div id="' + instanceId + '"></div>');

      component.instanceId = instanceId;

      var def = new $.Deferred();

      raintime.componentRegistry.setCallback(instanceId, function (component) {
        context.component.children.push(component);
        component.controller.context.parentInstanceId = context.instanceId;

        def.resolve(component.controller);
      });

      clientRenderer.requestComponent(component);

      return def;
    },

    /**
     * Adds listeners based on the given declaration.
     *
     * @override
     * @param {Map} [events=this.events]
     *
     * The map has the format <code>{ "event selector": "listener" }</code>. Listeners will be bound to the view,
     * with <code>this</code> set properly. Omitting the selector binds the event to <code>this.el</code>
     * (or <code>this.controller</code> if the event name starts with <code>view:</code>).
     */
    delegateEvents: function(events) {
      if (! (events || (events = _.result(this, 'events')))) { return; }

      var rainEvents = Object.keys(events).filter(startsWith('view:'));
      Backbone.View.prototype.delegateEvents.call(this, _.omit(events, rainEvents));

      if (this.isStarted()) {
        this._delegateRainEvents(_.object(rainEvents.map(function(e) {
          return [ e.replace('view:', ''), events[e] ];
        })));
      }

      function startsWith(prefix) {
        return function(str) {
          return (str.indexOf(prefix) === 0);
        };
      }
    },

    _delegateRainEvents: function(events) {
      Object.keys(events).forEach(function(e) {
        var listener = events[e];
        e = e.match(eventSplitter);
        var event = e[1], sid = e[3];

        if (typeof listener === 'string') {
          listener = this[listener];
        }

        listener = listener.bind(this);

        if (sid) {
          this.getChild(sid).then(addListener);
        } else {
          addListener(this);
        }

        function addListener(controller) {
          controller.on(event, listener);
        }
      }, this);
    }

  });

  RainView.extend = Backbone.View.extend;

  RainView.mixin = function(view, proto) {
    proto = proto || RainView.prototype;

    view = _.extend(view, proto);

    view._init();
    view._start();
  };

  return RainView;

});
